#include <iostream>
#include <regex>
#include <string>
#include <vector>

#define	GLOBAL_POLYNOM_ABBR 'x'
#define	GLOBAL_DEGREE_ABBR '^'

std::vector<std::string> data_resolve(std::vector<std::string> *v);

std::string delivative(std::string polynomial)
{
	std::string 										NOT_A_STRING = "";
	std::smatch											separated_data;

	std::regex 											validation("^[\\d\\+\\/\\-\\*x\\^]+$");  //// Manual change if changed *GLOBAL_POLYNOM_ABBR
	std::regex 											regex_for_data_separate("\\d+|[\\+\\-\\/\\*]|x\\^\\d+");  //// Manual change if changed *GLOBAL_POLYNOM_ABBR
	std::regex_token_iterator<std::string::iterator>	end_;
	std::regex_token_iterator<std::string::iterator>	curr_e(polynomial.begin(), polynomial.end(), regex_for_data_separate);
	std::vector<std::string>							data;
	
	if (!std::regex_search(polynomial, validation))
	{
		std::cout<<"Polynomial syntax error : code 0x1"<<std::endl;
		return (NOT_A_STRING);
	}
	else
	{
		if (curr_e != end_)
		{
			while (curr_e != end_) data.push_back(*curr_e++);
				data_resolve(&data);
			for(auto it : data)
				std::cout<<it<<std::endl;
		}
		else
			std::cout<<"Polynomial syntax error : code 0x1"<<std::endl;
	}
	return (polynomial);
}

int	main()
{
	std::string result;
	std::string input;

	std::getline(std::cin, input);

	result = delivative(input);
	
}

std::vector<std::string> data_resolve(std::vector<std::string> *v)
{
	int 		coefficient;
	auto		coefficient_mod = std::make_pair(false, 0);
	std::string data;

	coefficient = 1;
	for (auto k = v->begin(); k != v->end(); k++)
	{
			switch ((*k)[0])
			{
				case '/':
					coefficient_mod.first = true;
					coefficient_mod.second = 1;
					break ;
				case '*':
					coefficient_mod.first = true;
					coefficient_mod.second = 2;
					break;
				case GLOBAL_POLYNOM_ABBR:
					if ((*k)[1] == GLOBAL_DEGREE_ABBR)
					{
						int degree = stoi((*k).substr (2));
						if (degree <= 0)
							return {} ;
						else
						{
							data += std::to_string(coefficient_mod.first ? (coefficient_mod.second == 2 ? degree * coefficient : degree / coefficient) : degree)
								+ ((degree - 1 != 0) ? "x" : "") + ((degree - 1) > 1 ? ("^" + std::to_string(degree - 1)) : "");
							v->insert(k, data);
							v->erase(k + 1);
							coefficient_mod.first = false;
							coefficient_mod.second = 0;
							coefficient = 1;
						}
					}
					else
						return {} ;
					break ;
				default:
					if (isdigit((*k)[0]))
						coefficient = stoi(*k);
					break ;
			}
		data.clear();
	}
	return {} ;
}